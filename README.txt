Papa-figo is an 1v1 top-down game created to a game jam supported by Senac-PE and IGDA-Recife.
The game is driven by a local legend about a sick man (papa-figo, meaning the one who eats livers) that contracts a poor old man to kidnap children lost from their parents so the Papa-Figo can eat their livers and heal himself from the disease. It's a local history told by adults to frighten children who use to disobey their parents, so they get scary to get away from the parents and avoid to get lost and harmed.

The game was developed with Unty 5, Corel Draw, Photoshop and After Effects.

TEAM:
Abilio Nogueira - Programming
Everton Gonsaga - Programming
Otacilio Saraiva - Programming
Marcelino Borges - 2D artist and Programming 
Renata Melo - 2D artist
Emmanuel Peixoto - Music and FX

You can play here:
http://engenheirosdacomputacao.com.br/papafigos

Arts:
https://www.artstation.com/artwork/qGxXa