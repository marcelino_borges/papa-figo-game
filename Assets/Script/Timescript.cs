﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timescript : MonoBehaviour {

    Text text;
    public static float timeleft = 200f;
    public static float leg;
    // Use this for initialization
    void Start()
    {
        text = GetComponent<Text>();

    }

    // Update is called once per frame
    void Update()
    {
        timeleft -= Time.deltaTime;
        if (timeleft <= 0)
            timeleft = 0;
        text.text = "Time left: " + Mathf.Round(timeleft);
        leg = timeleft - Time.deltaTime;


    }
}