﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Imageupr : MonoBehaviour {
    public Sprite humanored;
    public Sprite semired;
    public Sprite lascadored;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if(Score.lifered < 0)
            SceneManager.LoadScene("VitoriaA");
        if (Score.lifered  < 50)
        {
            this.gameObject.GetComponent<SpriteRenderer>().sprite = lascadored;
        }
        else if (Score.lifered >= 50 && Score.lifered <= 99)
        {
            this.gameObject.GetComponent<SpriteRenderer>().sprite = semired;
        }
        else
        {
            this.gameObject.GetComponent<SpriteRenderer>().sprite = humanored;
            SceneManager.LoadScene("VitoriaV");
        }

    }
}
