﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public GameObject playerPrefab;
    List<GameObject> players = new List<GameObject>();
    //float speed = 3;
    private float maxspeed = 1;
    public  static int childCount1 = 0;
    public static int childCount2 = 0;
    public int maxchild = 30;
    //private float playerSpeedX;
    //private float playerSpeedY;
    //public AnimationCurve speeddecay;    

    private int MChild = 3;
    private Rigidbody2D rb2d;

    void Update()
    {
        foreach (GameObject player in GameObject.FindObjectsOfType(typeof(GameObject)))
        {
            if (player.tag == "Player" && !players.Contains(player))
                players.Add(player);
            
        }
        //player 1
        if (Input.GetKey(KeyCode.A))
            players[0].transform.Translate(Vector2.left * CurrentSpeed(childCount1) * CurrentSpeed(childCount1));
        if (Input.GetKey(KeyCode.D))
            players[0].transform.Translate(Vector2.right * CurrentSpeed(childCount1) * CurrentSpeed(childCount1));
        if (Input.GetKey(KeyCode.W))
            players[0].transform.Translate(Vector2.up * CurrentSpeed(childCount1) * CurrentSpeed(childCount1));
        if (Input.GetKey(KeyCode.S))
            players[0].transform.Translate(Vector2.down * CurrentSpeed(childCount1) * CurrentSpeed(childCount1));
        //player2
        if (Input.GetKey(KeyCode.LeftArrow))
            players[1].transform.Translate(Vector2.left * CurrentSpeed(childCount2) * CurrentSpeed(childCount2));
        if (Input.GetKey(KeyCode.RightArrow))
            players[1].transform.Translate(Vector2.right * CurrentSpeed(childCount2) * CurrentSpeed(childCount2));
        if (Input.GetKey(KeyCode.UpArrow))
            players[1].transform.Translate(Vector2.up * CurrentSpeed(childCount2) * CurrentSpeed(childCount2));
        if (Input.GetKey(KeyCode.DownArrow))
            players[1].transform.Translate(Vector2.down * CurrentSpeed(childCount2) * CurrentSpeed(childCount2));
    }
    //velocidade
        float  CurrentSpeed(int meuchild )
        {
        
        return maxspeed / (meuchild +2);
        }
    }

