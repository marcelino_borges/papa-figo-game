﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewBehaviourScript : MonoBehaviour {

    private static NewBehaviourScript instance;

   
	// Use this for initialization
	void Start () {
        DontDestroyOnLoad(gameObject);
        instance = this;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    public static NewBehaviourScript getIntance()
    {
        return instance;
    }
}
