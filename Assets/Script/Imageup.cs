﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Imageup : MonoBehaviour {
    public Sprite humanoazul;
    public Sprite semi;
    public Sprite lascado;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        if(ScoreA.lifeblue < 0)
            SceneManager.LoadScene("VitoriaV");
        if (ScoreA.lifeblue < 50)
        {
            this.gameObject.GetComponent<SpriteRenderer>().sprite = lascado;
        }
        else if (ScoreA.lifeblue >= 50 && ScoreA.lifeblue <= 99)
        {
            this.gameObject.GetComponent<SpriteRenderer>().sprite = semi;
        }
        else
        {
            this.gameObject.GetComponent<SpriteRenderer>().sprite = humanoazul;
            SceneManager.LoadScene("VitoriaA");
        }




    }
}
