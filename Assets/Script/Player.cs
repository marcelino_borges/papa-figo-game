﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

    public float maxspeed = 10;
    public int childCount = 1;

    private float playerSpeedX;
    private float playerSpeedY;
    public AnimationCurve speeddecay;

    private int MChild = 3;
    private Rigidbody2D rb2d;       
    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();

    }

    
    void FixedUpdate()
    {

        float dx = Input.GetAxis("Horizontal");
        float dy = Input.GetAxis("Vertical");

        GetComponent<Rigidbody2D>().velocity = new Vector2(dx * CurrentSpeed(), dy * CurrentSpeed());
       
    }
    float CurrentSpeed()
    {
        return maxspeed / (childCount +1);
    }
}