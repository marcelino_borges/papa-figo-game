﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawn : MonoBehaviour {
    public Transform[] spawnlocations;
    public List<int> currentspaws = new List<int>(); 
    public GameObject objectprefab;
    public bool botaotoscao;
	// Use this for initialization
	void Start () {
       
        for (int x = 0; x < 5; x++)
            spawnsomething();
		
	}


    void Update()
    {
        if (botaotoscao)
        {
            spawnsomething();
            botaotoscao = false;
        }
    }
    // Update is called once per frame
    public void spawnsomething() {
        int i = Random.Range(0, spawnlocations.Length);

        currentspaws.Add(i);

        GameObject newObj = Instantiate(objectprefab);
        newObj.transform.position = spawnlocations[i].position;

    }
}
