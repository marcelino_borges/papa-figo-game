﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Come : MonoBehaviour {
    public AudioSource fon;
	// Use this for initialization
	void Start () {
		
	}

    // Update is called once per frame
    void OnTriggerEnter2D(Collider2D other)
    {
        
        if(Controlplayer.childCount2 < 3)
        {
            if (other.gameObject.tag == "go")
            {
                Destroy(other.gameObject);
                Controlplayer.childCount2++;
                fon.Play();
            }
           
        }
    }
}