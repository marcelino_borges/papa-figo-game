﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controlplayer : MonoBehaviour {
    public GameObject playerPrefab;
    List<GameObject> players = new List<GameObject>();
    public float nspeed = 1;
    public static int childCount1 = 0;
    public static int childCount2 = 0;
    public Animator play1;
    public SpriteRenderer rendererplay1;
    public Animator play2;
    public SpriteRenderer rendererplay2;

    private void Start()
    {
        childCount1 = 0;
        childCount2 = 0;
    }

    void Update()
    {
        foreach (GameObject player in GameObject.FindObjectsOfType(typeof(GameObject)))
        {
            if (player.tag == "Playerb" && !players.Contains(player))
                players.Add(player);
        }

        foreach (GameObject player in GameObject.FindObjectsOfType(typeof(GameObject)))
        {
            if (player.tag == "Playera" && !players.Contains(player))
                players.Add(player);
        }

        if (Input.GetKey(KeyCode.A)) {
            players[0].transform.Translate(Vector2.left * CurrentSpeed(childCount1) * CurrentSpeed(childCount1));
            play2.SetBool("lado2", true);
            rendererplay2.flipX = false;
        }
        else
            play2.SetBool("lado2", false);


        if (Input.GetKey(KeyCode.D))
        {
            players[0].transform.Translate(Vector2.right * CurrentSpeed(childCount1) * CurrentSpeed(childCount1));
            play2.SetBool("lado2", true);
            rendererplay2.flipX = true;
        }

        if (Input.GetKey(KeyCode.W))
        {
            players[0].transform.Translate(Vector2.up * CurrentSpeed(childCount1) * CurrentSpeed(childCount1));
            play2.SetBool("cima2", true);
        }
        else
            play2.SetBool("cima2", false);


        if (Input.GetKey(KeyCode.S))
        {
            players[0].transform.Translate(Vector2.down * CurrentSpeed(childCount1) * CurrentSpeed(childCount1));
            play2.SetBool("baixo2", true);
        }
        else
            play2.SetBool("baixo2", false);

        //wasd
        if (Input.GetKey(KeyCode.LeftArrow)) { 
            players[1].transform.Translate(Vector2.left * CurrentSpeed(childCount2) * CurrentSpeed(childCount2));
            play1.SetBool("lado", true);
            rendererplay1.flipX = false;
        }
        else
            play1.SetBool("lado", false);

        if (Input.GetKey(KeyCode.RightArrow)) { 
            players[1].transform.Translate(Vector2.right * CurrentSpeed(childCount2) * CurrentSpeed(childCount2));
            play1.SetBool("lado", true);
            rendererplay1.flipX = true;
        }

        if (Input.GetKey(KeyCode.UpArrow)) { 
            players[1].transform.Translate(Vector2.up * CurrentSpeed(childCount2) * CurrentSpeed(childCount2));
            play1.SetBool("cima", true);
        }
        else
            play1.SetBool("cima", false);

        if (Input.GetKey(KeyCode.DownArrow)) { 
            players[1].transform.Translate(Vector2.down * CurrentSpeed(childCount2) * CurrentSpeed(childCount2));
            play1.SetBool("baixo", true);
        }
        else
            play1.SetBool("baixo", false);


    }
    float CurrentSpeed(float meuchild)
    {

        return nspeed / ((meuchild+3)/ 2);
    }
}

